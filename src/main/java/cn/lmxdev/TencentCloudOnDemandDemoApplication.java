package cn.lmxdev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TencentCloudOnDemandDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(TencentCloudOnDemandDemoApplication.class, args);
    }
}
