package cn.lmxdev.util;

import com.qcloud.vod.VodUploadClient;
import com.qcloud.vod.model.VodUploadRequest;
import com.qcloud.vod.model.VodUploadResponse;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.vod.v20180717.VodClient;
import com.tencentcloudapi.vod.v20180717.models.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TencentUtil {
    /**
     * 声明常用基础对象
     */
    private final Credential credential;
    private final VodClient vodClient;
    @Value("${tencent.secret-id}")
    private String secretId;
    @Value("${tencent.secret-key}")
    private String secretKey;

    /**
     * 构造方法，预先创建所需的基础对象
     */
    public TencentUtil() {
        HttpProfile httpProfile = new HttpProfile();
        ClientProfile clientProfile = new ClientProfile();
        httpProfile.setEndpoint("vod.tencentcloudapi.com");
        clientProfile.setHttpProfile(httpProfile);
        this.credential = new Credential(this.secretId, this.secretKey);
        this.vodClient = new VodClient(this.credential, "", clientProfile);
    }

    /**
     * 根据文件ID到腾讯云查询视频信息
     *
     * @param fileId 文件ID
     * @return 视频信息对象
     */
    public MediaInfo findVideo(String fileId) {
        try {
            this.credential.setSecretId(this.secretId);
            this.credential.setSecretKey(this.secretKey);
            DescribeMediaInfosRequest req = new DescribeMediaInfosRequest();
            req.setFileIds(new String[]{fileId});
            return this.vodClient.DescribeMediaInfos(req).getMediaInfoSet()[0];
        } catch (TencentCloudSDKException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 给指定视频进行转码操作
     *
     * @param fileId        文件ID
     * @param procedureName 转码工作流程称
     * @return 任务ID
     */
    public String videoProcessing(String fileId, String procedureName) {
        try {
            this.credential.setSecretId(this.secretId);
            this.credential.setSecretKey(this.secretKey);
            ProcessMediaByProcedureRequest req = new ProcessMediaByProcedureRequest();
            req.setFileId(fileId);
            req.setProcedureName(procedureName);
            return this.vodClient.ProcessMediaByProcedure(req).getTaskId();
        } catch (TencentCloudSDKException e) {
            return null;
        }
    }

    /**
     * 查询任务列表
     *
     * @return 任务列表
     */
    public TaskSimpleInfo[] selectTask() {
        try {
            this.credential.setSecretId(this.secretId);
            this.credential.setSecretKey(this.secretKey);
            DescribeTasksRequest req = new DescribeTasksRequest();
            return this.vodClient.DescribeTasks(req).getTaskSet();
        } catch (TencentCloudSDKException e) {
            return null;
        }
    }

    /**
     * 根据任务ID查询任务信息
     *
     * @param taskId 任务ID
     * @return 任务信息对象
     */
    public DescribeTaskDetailResponse findTask(String taskId) {
        try {
            this.credential.setSecretId(this.secretId);
            this.credential.setSecretKey(this.secretKey);
            DescribeTaskDetailRequest req = new DescribeTaskDetailRequest();
            req.setTaskId(taskId);
            return this.vodClient.DescribeTaskDetail(req);
        } catch (TencentCloudSDKException e) {
            return null;
        }
    }

    /**
     * 上传视频到腾讯云
     *
     * @param filePath 视频所在的路径
     * @return 文件ID
     */
    public String uploadVideo(String filePath) {
        try {
            VodUploadClient client = new VodUploadClient(this.secretId, this.secretKey);
            VodUploadRequest request = new VodUploadRequest();
            request.setMediaFilePath(filePath);
            VodUploadResponse response = client.upload("ap-guangzhou", request);
            return response.getFileId();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 根据文件ID删除在腾讯云中的视频
     *
     * @param fileId 文件ID
     */
    public void deleteVideo(String fileId) {
        try {
            this.credential.setSecretId(this.secretId);
            this.credential.setSecretKey(this.secretKey);
            DeleteMediaRequest req = new DeleteMediaRequest();
            req.setFileId(fileId);
            this.vodClient.DeleteMedia(req);
        } catch (TencentCloudSDKException ignored) {
        }
    }
}
