package cn.lmxdev.config;

import cn.lmxdev.dao.IVideoDao;
import cn.lmxdev.model.TVideo;
import cn.lmxdev.util.TencentUtil;
import com.tencentcloudapi.vod.v20180717.models.MediaInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;

@Configuration
@EnableScheduling
public class TencentVideoTask {
    @Autowired
    private TencentUtil tencentUtil;

    @Autowired
    private IVideoDao videoDao;

    /**
     * 上传视频任务，每十秒检测需要上传的视频
     */
    @Scheduled(fixedDelay = 10000)
    private void videoUploadTask() {
        System.out.println(LocalDateTime.now() + "：开始视频上传任务");

        // 查询数据库中等待上传的文件
        TVideo selectVideo = new TVideo();
        selectVideo.setState(1);
        List<TVideo> videoList = this.videoDao.select(null, null, selectVideo);
        for (TVideo thisVideo : videoList) {
            // 开始上传
            String fileId = tencentUtil.uploadVideo(thisVideo.getPath());

            // 删除本地文件
            if (new File(thisVideo.getPath()).delete())
                thisVideo.setPath(null);

            // 修改文件状态
            thisVideo.setFileId(fileId);
            thisVideo.setState(2);

            // 写入数据库
            this.videoDao.update(thisVideo);
        }

        System.out.println(LocalDateTime.now() + "：视频上传任务完成");
    }

    /**
     * 视频转码声明任务，每十秒检测等待转码的视频
     */
    @Scheduled(fixedDelay = 10000)
    private void videoProcessingTask() {
        System.out.println(LocalDateTime.now() + "：开始视频转码声明任务");

        // 查询数据库中等待转码的文件
        TVideo selectVideo = new TVideo();
        selectVideo.setState(2);
        List<TVideo> videoList = this.videoDao.select(null, null, selectVideo);
        for (TVideo thisVideo : videoList) {
            // 开始转码
            String taskId = tencentUtil.videoProcessing(thisVideo.getFileId(), thisVideo.getProcessName());

            // 修改文件状态
            thisVideo.setTaskId(taskId);
            thisVideo.setState(3);

            // 写入数据库
            this.videoDao.update(thisVideo);
        }

        System.out.println(LocalDateTime.now() + "：视频转码声明任务完成");
    }

    /**
     * 视频转码完成检测任务，每十秒检测一次正在转码中的视频是否转码完成
     */
    @Scheduled(fixedDelay = 10000)
    private void videoFinishTask() {
        System.out.println(LocalDateTime.now() + "：开始视频转码完成任务");

        // 查询数据库中等待转码的文件
        TVideo selectVideo = new TVideo();
        selectVideo.setState(3);
        List<TVideo> videoList = this.videoDao.select(null, null, selectVideo);
        for (TVideo thisVideo : videoList) {
            // 开始检测
            String status = this.tencentUtil.findTask(thisVideo.getTaskId()).getStatus();

            // 判断转码是否完成
            if ("FINISH".equals(status)) {
                // 获取视频信息
                MediaInfo mediaInfo = this.tencentUtil.findVideo(thisVideo.getFileId());

                // 修改文件状态
                thisVideo.setDuration(mediaInfo.getMetaData().getVideoDuration());
                thisVideo.setCoverUrl(mediaInfo.getBasicInfo().getCoverUrl());
                thisVideo.setTaskId(null);
                thisVideo.setState(4);

                // 写入数据库
                this.videoDao.update(thisVideo);
            }
        }

        System.out.println(LocalDateTime.now() + "：视频转码完成任务完成");
    }
}
