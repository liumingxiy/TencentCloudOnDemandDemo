package cn.lmxdev.controller;

import cn.lmxdev.dao.IVideoDao;
import cn.lmxdev.model.TVideo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping(value = "/video")
public class VideoController {
    @Value("${tencent.temp-video-path}")
    private String tempVideoPath;

    @Value("${tencent.app-id}")
    private String appId;

    @Autowired
    private IVideoDao videoDao;

    /**
     * 播放视频页面
     *
     * @param model
     * @param fileId
     * @return
     */
    @GetMapping(value = "/play")
    public String playPage(Model model,
                           @RequestParam(value = "fileId") String fileId) {

        // 传递数据
        model.addAttribute("fileId", fileId);
        model.addAttribute("appId", appId);

        return "video-play";
    }

    /**
     * 上传视频文件页面
     *
     * @return
     */
    @GetMapping(value = "/upload")
    public String uploadPage() {
        return "video-upload";
    }

    /**
     * 视频列表页面
     *
     * @param model
     * @param limit
     * @param page
     * @param video
     * @return
     */
    @GetMapping(value = "/list")
    public String listPage(Model model,
                           @RequestParam(value = "limit", required = false, defaultValue = "5") Integer limit,
                           @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                           TVideo video) {
        // 参数预处理
        if (video.getName() != null) video.setName(video.getName().trim());
        if (video.getProcessName() != null) video.setProcessName(video.getProcessName().trim());

        // 准备数据
        List<TVideo> videoList = this.videoDao.select((page - 1) * limit, limit, video);
        Integer dataCount = this.videoDao.count(video);
        int pageIndex = page;
        int pageCount = (int) Math.ceil((float) dataCount / limit);

        // 传递数据
        model.addAttribute("video", video);
        model.addAttribute("videoList", videoList);
        model.addAttribute("dataCount", dataCount);
        model.addAttribute("defaultLimit", limit);
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageCount", pageCount);

        return "video-list";
    }

    /**
     * 上传视频接口
     *
     * @param name
     * @param file
     * @return
     * @throws IOException
     */
    @PostMapping(value = "/upload")
    @ResponseBody
    public Map<String, Object> upload(@RequestParam(value = "name") String name,
                                      @RequestParam(value = "file") MultipartFile file) throws IOException {
        Map<String, Object> map = new HashMap<String, Object>();
        if (file != null) {
            TVideo video = new TVideo();

            video.setName(name);
            video.setSize(file.getSize());
            String fileName = file.getOriginalFilename();
            fileName = UUID.randomUUID() + "." + fileName.split("\\.")[fileName.split("\\.").length - 1];
            video.setPath(this.tempVideoPath + fileName);
            file.transferTo(new File(video.getPath()));

            if (videoDao.insert(video)) {
                map.put("msg", "上传成功");
            } else {
                new File(video.getPath()).deleteOnExit();
                map.put("msg", "上传失败");
            }
            return map;
        } else {
            map.put("msg", "请上传文件");
            return map;
        }
    }
}