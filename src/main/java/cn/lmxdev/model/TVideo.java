package cn.lmxdev.model;

import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class TVideo {
    private Integer id = null;
    private String name = null;
    private String path = null;
    private Long size = null;
    private String fileId = null;
    private String taskId = null;
    private Float duration = null;
    private String coverUrl = null;
    private String processName = null;
    private Integer state = null;
    private Date uploadTime = null;
    private Date createTime = null;
}
