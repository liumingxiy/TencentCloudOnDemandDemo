package cn.lmxdev.dao;

import cn.lmxdev.model.TVideo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface IVideoDao {
    /* 插入 */
    public boolean insert(@Param(value = "video") TVideo video);

    /* 删除 */
    public boolean delete(@Param(value = "id") Integer id);

    /* 更新 */
    public boolean update(@Param(value = "video") TVideo video);

    /* 查询 */
    public TVideo find(@Param(value = "id") Integer id);

    /* 计数 */
    public int count(@Param(value = "video") TVideo video);

    /* 查询列表 */
    public List<TVideo> select(@Param(value = "limitStart") Integer limitStart, @Param(value = "limitEnd") Integer limitEnd, @Param(value = "video") TVideo video);
}
