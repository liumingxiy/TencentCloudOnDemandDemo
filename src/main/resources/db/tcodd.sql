-- MySQL dump 10.13  Distrib 5.7.35, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: tcodd
-- ------------------------------------------------------
-- Server version	5.7.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `video`
--

DROP TABLE IF EXISTS `video`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video`
(
    `id`           int(11)      NOT NULL AUTO_INCREMENT COMMENT '主键',
    `name`         varchar(256) NOT NULL COMMENT '名称',
    `path`         varchar(256)          DEFAULT NULL COMMENT '本地路径',
    `size`         bigint(20)   NOT NULL COMMENT '大小',
    `file_id`      varchar(256)          DEFAULT NULL COMMENT '在腾讯云点播中的ID',
    `task_id`      varchar(256)          DEFAULT NULL COMMENT '在腾讯云点播中的任务ID',
    `duration`     float                 DEFAULT NULL COMMENT '视频时长，单位秒',
    `cover_url`    varchar(256)          DEFAULT NULL COMMENT '封面URL',
    `process_name` varchar(256) NOT NULL DEFAULT 'LongVideoPreset' COMMENT '视频转码任务名称',
    `state`        int(11)      NOT NULL DEFAULT '1' COMMENT '状态：1已经上传到本地、2已经上传到腾讯云点播、3已经转码中、4已经转码完成',
    `upload_time`  datetime              DEFAULT NULL COMMENT '上传时间',
    `create_time`  datetime     NOT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8 COMMENT ='视频表';
/*!40101 SET character_set_client = @saved_cs_client */;
